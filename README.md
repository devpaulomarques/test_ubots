# Ubots teste

**Follow these steps to execute**

1. git clone https://devpaulomarques@bitbucket.org/devpaulomarques/test_ubots.git
2. cd test_ubots
3. npm install
4. npm start (run localhost:3000)
5. do get requests using Postman to url's below

    - http://localhost:3000/clients/highOrderClients
    - http://localhost:3000/clients/highOrderLastYear
    - http://localhost:3000/clients/mostLoyalCustomer
    - http://localhost:3000/clients/recomendationByClient/:id (000.000.000-08)