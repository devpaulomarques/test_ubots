'use strict';

var config = require('../config');
var request = require("request");
var moment = require('moment');

moment.locale('pt-BR');

var utils = {};
utils.clientId = undefined;

utils.getRandomRecommendation = (json) => {
    return new Promise((resolve, reject) => {
        var order = json[0].orders[Math.floor(Math.random() * json[0].orders.length)];
        var product = order.itens[Math.floor(Math.random() * order.itens.length)];
        json[0].recommendation = product;

        resolve(json);
    })
}

utils.filterByClientId = (json) => {
    return new Promise((resolve, reject) => {
        resolve(json.filter(client => client.cpf.replace('-', '.') == utils.clientId.replace('-', '.')));
    })
}

utils.mergeData = (clients, orders) => {
    return new Promise((resolve, reject) => {
        resolve(JSON.parse(clients).map(client => {
            client.orders = JSON
                .parse(orders)
                .filter(order => order.cliente.slice(-1) == client.cpf.slice(-1));
            return client;
        }));
    });
}

utils.getClients = () => {
    return new Promise((resolve, reject) => {
        request(config.clients, (error, response, clients) => {
            if (error) 
                reject(error);
            resolve(clients);
        });
    });
}

utils.getOrders = (clients) => {
    return new Promise((resolve, reject) => {
        request(config.orders, (error, response, orders) => {
            if (error) 
                reject(error);
            resolve(utils.mergeData(clients, orders));
        });
    });
}

utils.getValorTotal = (json) => {
    return new Promise((resolve, reject) => {
        json.map(client => {
            client.valorTotal = Number(client.orders.map(order => order.valorTotal).reduce((p, c) => p + c).toFixed(2));
        });

        resolve(json);
    })
}

utils.filterHighOrder = (json) => {
    return new Promise((resolve, reject) => {
        resolve(json.filter(f => f.orders[0].valorTotal == Math.max.apply(null, json.map(n => {
            return n.orders[0].valorTotal
        }))));
    })
}

utils.filterHighOrderByClient = (json) => {
    return new Promise((resolve, reject) => {
        json.map(client => {
            client.orders = client
                .orders
                .filter(order => order.valorTotal == Math.max.apply(null, client.orders.map(n => {
                    return n.valorTotal;
                })));
        })

        resolve(json);
    })
}

utils.filterByYear = (json) => {
    return new Promise((resolve, reject) => {
        json.map(client => {
            client.orders = client
                .orders
                .filter(order => moment(order.data, "DD-MM-YYYY").year() == moment(new Date()).year() - 1)
        })

        resolve(json);
    })
}

utils.sortByValorTotalDesc = (json) => {
    return new Promise((resolve, reject) => {
        json.sort((a, b) => {
            return (b.valorTotal > a.valorTotal)
                ? 1
                : ((a.valorTotal > b.valorTotal)
                    ? -1
                    : 0);
        });

        resolve(json);
    })
}

utils.getTotalOrders = (json) => {
    return new Promise((resolve, reject) => {

        json.map(client => client.orderCount = client.orders.length);

        resolve(json);
    })
}

utils.sortByTotalOrders = (json) => {
    return new Promise((resolve, reject) => {
        json.sort((a, b) => {
            return (b.orderCount > a.orderCount)
                ? 1
                : ((a.orderCount > b.orderCount)
                    ? -1
                    : 0);
        });

        resolve(json);
    })
}

module.exports = utils;