'use strict';

var ctrl_client = {};

var config = require('../config');
var util = require('../lib/util');

ctrl_client.highOrderClients = (req, res, next) => {
    util.getClients()
        .then(util.getOrders)
        .then(util.getValorTotal)
        .then(util.sortByValorTotalDesc)
        .then(json => res.json(json));
};

ctrl_client.highOrderLastYear = (req, res, next) => {

    util.getClients()
        .then(util.getOrders)
        .then(util.filterByYear)
        .then(util.filterHighOrderByClient)
        .then(util.filterHighOrder)
        .then(json => res.json(json));
};

ctrl_client.mostLoyalCustomer = (req, res, next) => {
    util.getClients()
        .then(util.getOrders)
        .then(util.getTotalOrders)
        .then(util.sortByTotalOrders)
        .then(json => res.json(json));
};

ctrl_client.recomendationByClient = (req, res, next) => {
    util.clientId = req.params.id;
    util.getClients()
        .then(util.getOrders)
        .then(util.filterByClientId)
        .then(util.getRandomRecommendation)
        .then(json => res.json(json));
};



module.exports = ctrl_client;

module.exports = app => {
    app.get('/clients/highOrderClients', ctrl_client.highOrderClients);
    app.get('/clients/highOrderLastYear', ctrl_client.highOrderLastYear);
    app.get('/clients/mostLoyalCustomer', ctrl_client.mostLoyalCustomer);
    app.get('/clients/recomendationByClient/:id', ctrl_client.recomendationByClient);
};