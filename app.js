'use strict';

global.WebSocket = require('ws');

var methodOverride = require('method-override');
var errorHandler = require('./lib/errorHandler.js');
var bodyParser = require('body-parser');
var express = require('express');
var cors = require('cors');
var logger  = require('morgan');
var config = require('./config');

config.root = __dirname;

var app = express();
app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());

if (config.env === 'development') {
  app.use(logger('dev'));
}

require('./config/routes')(app);

app.use(errorHandler());

module.exports = app;