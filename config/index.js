/**
 * configuration file
 *
 */

'use strict';

var _ = require('lodash');

var config = (function (env) {
  var conf = {};

  // Common configuration
  conf.common = {
    app: {
      name: "rest-api"
    },
    root: "",
    clients: 'https://mockbin.com/bin/4b9d2289-d331-4dfd-bdd4-8e7c55df0f55',
    orders: 'https://mockbin.com/bin/2f40e68e-4793-4d2c-a466-450784ecf7c1',
    port: process.env.PORT || 3000
  };

  // Development configuration
  conf.development = {
    env: "development"
  };

  // Test configuration
  conf.test = {
    env: "test",
    port: process.env.PORT || 3030
  };

  // Production configuration
  conf.production = {
    env: "production"
  };

  return _.merge(conf.common, conf[env]);

})(process.env.NODE_ENV || 'development');

module.exports = config;